
//
//  Entry.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import Foundation

struct Album: Codable {
    
    var id: Int?
    var title: String?
    
}
