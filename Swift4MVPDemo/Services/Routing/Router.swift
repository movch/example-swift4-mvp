//
//  Router.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import UIKit

class Router {
    
    let navigationController: UINavigationController?
    let viewControllerFactory: ViewControllerFactory
    
    init(navigationController: UINavigationController?, viewControllerFactory: ViewControllerFactory) {
        self.navigationController = navigationController
        self.viewControllerFactory = viewControllerFactory
    }
    
    func presentPhotos(forAlbum album: Album) {
        let vc = viewControllerFactory.createPhotosViewController(forAlbum: album)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
