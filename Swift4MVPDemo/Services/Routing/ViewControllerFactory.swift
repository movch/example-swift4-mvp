//
//  ViewControllerFactory.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import UIKit

class ViewControllerFactory {
    
    let storyboard: UIStoryboard
    
    init(storyboard: UIStoryboard) {
        self.storyboard = storyboard
    }
    
    func createPhotosViewController(forAlbum album: Album) -> PhotosViewController {
        let vc = storyboard.instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
        vc.album = album
        return vc
    }
    
}
