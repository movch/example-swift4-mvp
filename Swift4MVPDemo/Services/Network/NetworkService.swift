//
//  NetworkService.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 23/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import Foundation

public class NetworkService {
    
    /**
     Perform URLRequest and decode JSON using Codable protocol.
     
     Usage example:
     
         let urlRequest = URLRequest(url: URL(string: "http://jsonplaceholder.typicode.com/users/1")!)
         NetworkService().perform(request: urlRequest, completion: { (result: User?, error) -> () in
             print(result!.name)
         })
     */
    
    public func perform<T: Codable>(request: URLRequest, completion: @escaping (_ result: T?, _ error: Error?) -> ()) {
        let task = URLSession.shared.dataTask(with: request) { (data, _, error) -> () in
            if error == nil, let receivedData = data {
                do {
                    let jsonDecoder = JSONDecoder()
                    let object = try jsonDecoder.decode(T.self, from: receivedData)
                    completion(object, nil)
                } catch let parsingError {
                    completion(nil, parsingError)
                }
            } else {
                completion(nil, error)
            }
        }
        
        task.resume()
    }
    
}
