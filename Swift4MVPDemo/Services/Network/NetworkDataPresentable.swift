//
//  NetworkDataPresentable.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import UIKit

public protocol NetworkDataPresentable: NSObjectProtocol {
    
    func beginLoading()
    func finishLoading(error: Error?)
    func setupData(data: Any?)
    
}

extension NetworkDataPresentable where Self: UIViewController {
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        NetworkActivityIndicator.shared.show(view)
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        NetworkActivityIndicator.shared.hide()
    }
    
    func showError(error: Error?) {
        if error != nil {
            let alert = UIAlertController(
                title: "Error", message: error?.localizedDescription,
                preferredStyle: UIAlertControllerStyle.alert
            )
            alert.addAction(UIAlertAction(
                title: "Ok",
                style: UIAlertActionStyle.cancel,
                handler: nil)
            )
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func beginLoading() {
        showActivityIndicator()
    }
    
    func finishLoading(error: Error?) {
        hideActivityIndicator()
        showError(error: error)
    }
    
    func setupData(data: Any?) {
    }
    
}
