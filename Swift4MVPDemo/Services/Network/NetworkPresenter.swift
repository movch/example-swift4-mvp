//
//  NetworkPresenter.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import Foundation

public class NetworkPresenter {
    
    public var service = NetworkService()
    public weak var viewController: NetworkDataPresentable?
    
    init(viewController: NetworkDataPresentable) {
        self.viewController = viewController
    }
    
    init(viewController: NetworkDataPresentable, service: NetworkService) {
        self.viewController = viewController
        self.service = service
    }
    
    public func perform<T: Codable>(request: URLRequest) -> T? {
        viewController?.beginLoading()
        
        service.perform(request: request) { [weak self](result: T?, error) -> () in
            if error == nil, let receivedObject = result {
                self?.didFinishRequest(with: receivedObject)
             } else {
                self?.didFinishRequest(with: error)
            }
        }
        
        return nil
    }
    
    /**
     Method is being called when request has been completed successfully
     */
    public func didFinishRequest(with data: Any) {
        didFinishRequest()
        viewController?.finishLoading(error: nil)
        willSetData(data: data)
        viewController?.setupData(data: data)
        didSetData(data: data)
    }
    
    /**
     Method is being called when request has been completed with errors
     */
    public func didFinishRequest(with error: Error?) {
        didFinishRequest()
        viewController?.finishLoading(error: error)
    }
    
    /**
     Method is being called before *finishLoading*
     */
    public func didFinishRequest() {
    }
    
    /**
     Method is being called before *setupData*
     */
    public func willSetData(data: Any) {
    }
    
    /**
     Method is being called after *setupData*
     */
    public func didSetData(data: Any) {
    }

}
