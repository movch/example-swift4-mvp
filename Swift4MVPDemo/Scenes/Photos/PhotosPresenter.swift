//
//  PhotosPresenter.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import Foundation

final class PhotosPresenter: NetworkPresenter {
    
    var photos: [Photo] = []
    
    func getPhotos(forAlbum albumID: Int) {
        guard let url = URL(string: "\(NetworkSettings.serverURL)/photos?albumId=\(albumID)") else { return }
        let request = URLRequest(url: url)
        let _: [Photo]? = perform(request: request)
    }
    
    override func willSetData(data: Any) {
        if let receivedPhotos = data as? [Photo] {
            photos = receivedPhotos
        }
    }
    
}
