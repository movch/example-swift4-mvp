//
//  PhotosViewController.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import UIKit

final class PhotosViewController: UITableViewController {
    
    var album: Album?
    lazy var presenter: PhotosPresenter = PhotosPresenter(viewController: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Photos"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let album = self.album else { return }
        title = album.title
        guard let albumID = album.id else { return }
        presenter.getPhotos(forAlbum: albumID)
    }
    
}

// MARK: - NetworkDataPresentable

extension PhotosViewController: NetworkDataPresentable {
    
    func setupData(data: Any?) {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
}

// MARK: - Table view data source

extension PhotosViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoCell", for: indexPath)
        cell.textLabel?.text = presenter.photos[indexPath.row].title
        return cell
    }
    
}
