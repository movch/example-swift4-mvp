//
//  TodayTableViewController.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 23/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import UIKit

final class AlbumsViewController: UITableViewController {
    
    lazy var presenter: AlbumsPresenter = AlbumsPresenter(viewController: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Albums"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.getAlbums()
    }

}

// MARK: - NetworkDataPresentable

extension AlbumsViewController: NetworkDataPresentable {
    
    func setupData(data: Any?) {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
}

// MARK: - Table view data source

extension AlbumsViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.albums.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "entryCell", for: indexPath)
        cell.textLabel?.text = presenter.albums[indexPath.row].title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let factory = ViewControllerFactory(storyboard: UIStoryboard(name: "Main", bundle: nil))
        let router = Router(
            navigationController: navigationController,
            viewControllerFactory: factory
        )
        let album = presenter.albums[indexPath.row]
        router.presentPhotos(forAlbum: album)
    }
}
