//
//  TodayPresenter.swift
//  Swift4MVPDemo
//
//  Created by Michael Ovchinnikov on 24/09/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

import Foundation

final class AlbumsPresenter: NetworkPresenter {
    
    var albums: [Album] = []
    
    func getAlbums() {
        guard let url = URL(string: "\(NetworkSettings.serverURL)/albums") else { return }
        let request = URLRequest(url: url)
        let _: [Album]? = perform(request: request)
    }
    
    override func willSetData(data: Any) {
        if let receivedAlbums = data as? [Album] {
            albums = receivedAlbums
        }
    }
    
}
